# Hubspot local module preview

This is a POC to create a local module development environment, using the [Hubspot local development tools](https://designers.hubspot.com/docs/tools/local-development).

## Why this is a POC?

* I didn't have time to create all the hubl functions, filters and variables
* Only single fields are supported, without conditionals
* No HMR, becuase I couldn't find a clean way to do it.

## What can it do?

* Pull a single directory from the Hubspot design tools.
* Run a local server with links to all the modules. 
* Add value(s) to field inputs to get them displayed.
* Push the module changes to Hubspot.

## What can be added?

All your favorite development tools, like sass/less/postcss, autoprefixer, babel, webpack/rollup/parcel.

Put it in git to make the revision history a lot more meaningful than Hubspots revisions.

Anything you can think of.

## How to use it?

1. Make a directory, and go inside
2. Run `npx degit bitbucket:davidDuymelinckMia/hubspot-local-module-preview`
3. Copy env.example and hubspot.config.example.yml without example in the filename, and add your values
4. Run `npm install`
5. Run `npm hs:pull -- --portal=DEV cms-project`. The portal comes from the hubspot.config.yml and cms-project is the Hubspot design tools directory.
6. Run `npm preview` and go to the local server url.
7. Start developing locally.
8. Run `npm hs:push` to get the changes on Hubspot.