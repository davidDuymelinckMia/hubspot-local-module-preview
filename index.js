const polka = require('polka');
const { readdirSync, readFileSync } = require('fs');
const nunjucks = require('nunjucks');
const dotenv = require('dotenv');

const hubl = new nunjucks.Environment();

dotenv.config();

const getDirectories = source =>
    readdirSync(source, { withFileTypes: true })
        .filter(dirent => dirent.isDirectory())
        .map(dirent => dirent.name);

let server = polka();
const modules = getDirectories(process.env.MODULE_DIR);
const baseUrl = `http://${process.env.DOMAIN}:${process.env.PORT}`;

server.get('/', (req,res) =>{
    let links = [];

    for(let module of modules) {
        let moduleLink = module.replace('.module', '');
        links.push(`<a href="${baseUrl}/${moduleLink}">${moduleLink}</a>`);
    }

    res.end(links.join('<br>'));
});

for(let dir of modules){
    server.get('/'+dir.replace('.module', ''), (req, res) => {
        const style = readFileSync(`${process.env.MODULE_DIR}/${dir}/module.css`, 'utf8');
        const js = readFileSync(`${process.env.MODULE_DIR}/${dir}/module.js`, 'utf8');

        let links = [];

        for(let module of modules) {
            let moduleLink = module.replace('.module', '');
            links.push(`<a href="${baseUrl}/${moduleLink}">${moduleLink}</a>`);
        }

        const navigation = links.join('<br>');

        const moduleHtml = readFileSync(`${process.env.MODULE_DIR}/${dir}/module.html`, 'utf8');
        let hublOptions = { "module": {}};

        if(req.query) {
            hublOptions.module = req.query;
        }

        const body = hubl.renderString(moduleHtml, hublOptions);
        const fields = JSON.parse(readFileSync(`${process.env.MODULE_DIR}/${dir}/fields.json`, 'utf8'));
        let fieldsHTML = '';

        if(fields.length > 0) {
            let fieldsInput = [];

            for(let field of fields) {
                fieldsInput.push(`<label>${field.label} <input name="${field.name}"></label>`);
            }

            fieldsHTML = `
            <form action="">
            <h2>Fields</h2>
            ${fieldsInput.join('<br>')}
            <button type="submit">Update</button>
            </form>
            `;
        }

        let html = `
        <!doctype html>
        <html>
            <head>
                <style>${style}</style>
                <script>${js}</script>
            </head>
            <body>
            <nav>${navigation}</nav>
            <hr>
            ${body}
            <hr>
            ${fieldsHTML}
            </body>
        </html>
        `;

        res.end(html);
    })
}

server.listen(process.env.PORT, err => {
    if (err) throw err;
    console.log(`> Running on ${process.env.DOMAIN}:${process.env.PORT}`);
});